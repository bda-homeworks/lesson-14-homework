import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
public class Maion {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the date (yyyy-MM-dd)");
        String userInput = input.nextLine();

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(userInput, format);

        int year = date.getYear();
        int month = date.getMonthValue();
        int day = date.getDayOfMonth();

        int differentYear = year % 100;

        System.out.println(differentYear + "/" + month + "/" + day);

        input.close();
    }
}
