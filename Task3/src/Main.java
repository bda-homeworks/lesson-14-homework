import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the date yyyy-MM-dd");
        String userInput = scanner.next();
        LocalDate date = LocalDate.parse(userInput);

        int dayOfYear = date.get(ChronoField.DAY_OF_YEAR);

        System.out.println("Day of the year: " + dayOfYear);

        scanner.close();
    }
}
